# Expense Reimbursement System

## Description
This is an Expense Reimbursement System where employees can request an expense reimbursement, giving a description, a type, and the dollar amount that they need resolved. Managers will see these reimbursements 

## Technologies Used
* Postgresql - version 42.2.18
* Java - version 1.8.0_271
* Junit - version 4.13
* Log4j - version 1.2.17

## Features

* Sign in as an employee or manager
* See your list of reimbursements you have submitted if you are an employee
* Employees can submit a new reimbursement giving it a type, description, and dollar amount
* Managers can view every reimbursement regardless of who made it
* Managers can filter reimbursements by status
* Managers can approve or deny pending reimbursements

## Getting Started

* clone the repository using 'git clone https://gitlab.com/james_beighley/project-1'
* either set up your environment variables for the database url, username and password or paste them from your database inside the url, username and password variables inside the src/main/java/com/dao/ReimbursementDaoImpl.java file and the src/main/java/com/dao/UserDaoImpl.java file.
* Start the backend tomcat server in Spring Tool Suite
* Navigate to localhost/PORT_NUMBER/Project-1

## Usage

First sign in on the landing page with an employee username and password. If successful, the next page will display your reimbursements. Click the New Reimbursement in the top left corner which will give you a form where you can type in the specifics of your new request, make sure the dollar amount is an integer. This will forward you back to the list of your reimbursements. Sign out when you are done. similarly, sign in as a manager and you will see a similar list page with everyones reimbursements. The top tabs will have filters for status types, and another for clearing reimbursements, which will bring you to a form where you can type in the reimbursement you want to resolve and the status you will give it. You will be forwarded back to the list of reimbursements and you should see the change in the list. You can sign out when you are done.
