import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.service.ReimbursementServiceImpl;

public class ReimbursementServiceTest {

	private static ReimbursementServiceImpl service = new ReimbursementServiceImpl();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void createReimbursementTest() {
		int oldsize = service.getAllReimbursements().size();
		service.createReimbursement(10, "description", "LODGING", 1);
		assertEquals(oldsize+1, service.getAllReimbursements().size());
	}
	
	@Test
	public void getAllTest() {
		assertTrue(service.getAllReimbursementsWithStatus("PENDING").size()<service.getAllReimbursements().size());
		assertTrue(service.getAllReimbursementsWithStatus("trevinchester@gmail.com").size()<service.getAllReimbursements().size());


	}
	

	@Test
	public void getStatusIdTest() {
		assertEquals(7,service.getStatusId(10));
		assertEquals(9,service.getStatusId(12));
		assertEquals(0,service.getStatusId(-1));
	}
	
	
	@Test
	public void getReimbursementsByEmailTest() {
		assertEquals(service.getReimbursementsByEmail("trevinchester@gmail.com").size(),1);
		assertEquals(service.getReimbursementsByEmail("someunregisteredemail").size(),0);
	}
	
	@Test
	public void getAllReimbursementsWithStatusTest() {
		assertTrue(service.getAllReimbursementsWithStatus("PENDING").size()<service.getAllReimbursements().size());
	}
	


}