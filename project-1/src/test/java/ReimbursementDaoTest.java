import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.dao.ReimbursementDao;
import com.dao.ReimbursementDaoImpl;

public class ReimbursementDaoTest {

	private static ReimbursementDao dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new ReimbursementDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		dao.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		dao.h2DestroyDao();
	}

	@Test
	public void getStatusIdTest() {
		assertEquals(1, dao.getStatusId(1));
	}


}
