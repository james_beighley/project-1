window.onload = function() {
	
	//I don't want to add an event listener because the page load IS THE EVENT
	//in your project 1, you'll be going to the server here to get the session data...because THAT is who
	// is currently logged in
	getAllReimbursements();
}

let myUserInformation ={};

function getAllReimbursements() {
	fetch(
			'http://localhost:9001/project-1/api/Ajax/pending')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				myUserInformation=daSecondResponse;
				ourDOMManipulation(daSecondResponse);
			})

}

function ourDOMManipulation(ourJSON) {

	for (let i = 0; i < ourJSON.length; i++) {
		// all creations
		let newTR = document.createElement("tr");
		let newTD0 = document.createElement("td");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
		let newTD5 = document.createElement("td");
		let newTD6 = document.createElement("td");
		let newTD7 = document.createElement("td");
		let newTD8 = document.createElement("td");
		
		let date1 = new Date(ourJSON[i].reimb_submitted);
		let date2 = new Date(ourJSON[i].reimb_resolved);
		date1.toLocaleString();
		
		// population creations
		let myText1 = document.createTextNode(ourJSON[i].reimb_id);
		let myText2 = document.createTextNode(ourJSON[i].reimb_amount);
		let myText3 = document.createTextNode(date1.toLocaleString());
		let myText4 = document.createTextNode(date2.toLocaleString());
		myText4 = document.createTextNode("none");
		let myText5 = document.createTextNode(ourJSON[i].reimb_description);
		let myText6 = document.createTextNode(ourJSON[i].reimb_author);
		let myText7 = document.createTextNode(ourJSON[i].reimb_resolver);
		let myText8 = document.createTextNode(ourJSON[i].reimb_status_id);
		let myText9 = document.createTextNode(ourJSON[i].reimb_type_id);
		//newDiv.appendChild(divText);
		
		
		///all appendings
		newTD0.appendChild(myText1);
		newTD1.appendChild(myText2);
		newTD2.appendChild(myText3);
		newTD3.appendChild(myText4);
		newTD4.appendChild(myText5);
		newTD5.appendChild(myText6);
		newTD6.appendChild(myText7);
		newTD7.appendChild(myText8);
		newTD8.appendChild(myText9);
		
		newTR.appendChild(newTD0);
		newTR.appendChild(newTD1);
		newTR.appendChild(newTD2);
		newTR.appendChild(newTD3);
		newTR.appendChild(newTD4);
		newTR.appendChild(newTD5);
		newTR.appendChild(newTD6);
		newTR.appendChild(newTD7);
		newTR.appendChild(newTD8);
		
		let newSelectionTwo = document.querySelector("#villTableBody");
		newSelectionTwo.appendChild(newTR);
	}
}