package com.model;
import java.sql.Timestamp;
public class Reimbursement {
	private int reimb_id;
	private int reimb_amount;
	private Timestamp reimb_submitted;
	private Timestamp reimb_resolved;
	private String reimb_description;
	private String reimb_author;
	private int reimb_resolver;
	private String reimb_status_id;
	private String reimb_type_id;
	public Reimbursement(int r_i, int r_a, Timestamp r_s, Timestamp r_r, String r_d, String a, int r, String s, String t) {
		setReimb_id(r_i);
		setReimb_amount(r_a);
		setReimb_submitted(r_s);
		setReimb_resolved(r_r);
		setReimb_description(r_d);
		setReimb_author(a);
		setReimb_resolver(r);
		setReimb_status_id(s);
		setReimb_type_id(t);
	}
	public int getReimb_id() {
		return reimb_id;
	}
	public void setReimb_id(int reimb_id) {
		this.reimb_id = reimb_id;
	}
	public int getReimb_amount() {
		return reimb_amount;
	}
	public void setReimb_amount(int reimb_amount) {
		this.reimb_amount = reimb_amount;
	}
	public Timestamp getReimb_submitted() {
		return reimb_submitted;
	}
	public void setReimb_submitted(Timestamp reimb_submitted) {
		this.reimb_submitted = reimb_submitted;
	}
	public Timestamp getReimb_resolved() {
		return reimb_resolved;
	}
	public void setReimb_resolved(Timestamp reimb_resolved) {
		this.reimb_resolved = reimb_resolved;
	}
	public String getReimb_description() {
		return reimb_description;
	}
	public void setReimb_description(String reimb_description) {
		this.reimb_description = reimb_description;
	}
	public String getReimb_author() {
		return reimb_author;
	}
	public void setReimb_author(String reimb_author) {
		this.reimb_author = reimb_author;
	}
	public int getReimb_resolver() {
		return reimb_resolver;
	}
	public void setReimb_resolver(int reimb_resolver) {
		this.reimb_resolver = reimb_resolver;
	}
	public String getReimb_status_id() {
		return reimb_status_id;
	}
	public void setReimb_status_id(String reimb_status_id) {
		this.reimb_status_id = reimb_status_id;
	}
	public String getReimb_type_id() {
		return reimb_type_id;
	}
	public void setReimb_type_id(String reimb_type_id) {
		this.reimb_type_id = reimb_type_id;
	}
}
