package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.model.Reimbursement;
import com.model.User;

public class ReimbursementDaoImpl implements ReimbursementDao {
	
	String url = "";
	String username = "";
	String password = "";
	
	
	static {
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("try static block has fired");
        } catch (ClassNotFoundException e) {
            System.out.println("THE STATIC BLOCK HAS FAILED");
            e.printStackTrace();
        }
    }


	
	public ReimbursementDaoImpl() {
		
	}
	
	public ReimbursementDaoImpl(String _url, String _username, String _password) {
		url= _url;
		username = _username;
		password = _password;
	}

	
	@Override
	public void createReimbursement(int amount, String description, String type, int authorid) {
		// TODO Auto-generated method stub
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "WITH ins1 AS ( INSERT INTO Ers_Reimbursement_type(ers_type_id, reimb_type) VALUES (default, " + "'" +type+ "'" + ") RETURNING ers_type_id AS sample_id), ins2 AS (\n" + 
					"    INSERT INTO Ers_Reimbursement_Status (id, reimb_status) values (default, 'PENDING') returning id as generated_reimb_id)\n" + 
					"	INSERT INTO Ers_Reimbursement (reimb_id, reimb_amount, reimb_submitted, reimb_resolved, reimb_description,reimb_author,reimb_resolver,reimb_status_id,reimb_type_id)\n" + 
					"	values(default, "+ amount +", CURRENT_TIMESTAMP, null, " + "'" +description+ "'" + ", "+ authorid +", null, (select generated_reimb_id from ins2), (select sample_id from ins1));";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public ArrayList<Reimbursement> getAllReimbursements() {
		ArrayList<Reimbursement> toreturn = new ArrayList<Reimbursement>();
		
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "SELECT * FROM ers_reimbursement_statuses_types_authors";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			//int u_i, String u, String p, String f, String l, String e, String r_i
			while(rs.next()) {
				int r_i = rs.getInt(1);
				int r_a = rs.getInt(2);
				Timestamp submitted = rs.getTimestamp(3);
				Timestamp resolved = rs.getTimestamp(4);
				String description = rs.getString(5);
				String authorid = rs.getString(15);
				int resolverid = rs.getInt(7);
				String statusid = rs.getString(11);
				String typeid = rs.getString(13);
				
				toreturn.add(new Reimbursement(r_i, r_a, submitted, resolved, description, authorid, resolverid, statusid, typeid));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	
		return toreturn;
	}
	
	public ArrayList<Reimbursement> getAllReimbursementsWithStatus(String status) {
		ArrayList<Reimbursement> toreturn = new ArrayList<Reimbursement>();
		
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "SELECT * FROM ers_reimbursement_statuses_types_authors WHERE reimb_status =" + "'" +status+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			//int u_i, String u, String p, String f, String l, String e, String r_i
			while(rs.next()) {
				int r_i = rs.getInt(1);
				int r_a = rs.getInt(2);
				Timestamp submitted = rs.getTimestamp(3);
				Timestamp resolved = rs.getTimestamp(4);
				String description = rs.getString(5);
				String authorid = rs.getString(15);
				int resolverid = rs.getInt(7);
				String statusid = rs.getString(11);
				String typeid = rs.getString(13);
				
				toreturn.add(new Reimbursement(r_i, r_a, submitted, resolved, description, authorid, resolverid, statusid, typeid));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	
		return toreturn;
	}
	
	public void resolveReimbursement(int reimbursementid, int resolverid, String resolved, int status_id) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "UPDATE ers_reimbursement SET reimb_resolver=" + "'" +resolverid  + "'" + ", reimb_resolved = CURRENT_TIMESTAMP WHERE reimb_id = "+ "'" +reimbursementid +"'";
			
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
			sql = "UPDATE Ers_Reimbursement_Status SET reimb_status=" + "'" +resolved  + "'" + " WHERE id = "+ "'" +status_id +"'";
			
			PreparedStatement ps1 = conn.prepareStatement(sql);
			
			ps1.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int getStatusId(int reimbursementid) {
		int id = 0;
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_id =" + "'" +reimbursementid+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			//int u_i, String u, String p, String f, String l, String e, String r_i
			while(rs.next()) {
				id = rs.getInt(8);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	
		return id;
	}

	@Override
	public ArrayList<Reimbursement> getReimbursementsByEmail(String email) {
		ArrayList<Reimbursement> toreturn = new ArrayList<Reimbursement>();
		
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "SELECT * FROM ers_reimbursement_statuses_types_authors WHERE ers_reimbursement_statuses_types_authors.author__email =" + "'" +email+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			//int u_i, String u, String p, String f, String l, String e, String r_i
			while(rs.next()) {
				int r_i = rs.getInt(1);
				int r_a = rs.getInt(2);
				Timestamp submitted = rs.getTimestamp(3);
				Timestamp resolved = rs.getTimestamp(4);
				String description = rs.getString(5);
				String authorid = rs.getString(15);
				int resolverid = rs.getInt(7);
				String statusid = rs.getString(11);
				String typeid = rs.getString(13);
				
				toreturn.add(new Reimbursement(r_i, r_a, submitted, resolved, description, authorid, resolverid, statusid, typeid));
				}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	
		return toreturn;
	}
	
	@Override
	public void h2InitDao() {
		
		try(Connection conn=
				DriverManager.getConnection(url,username, password))
		{
			String sql= ""+
			"create table Ers_User_Roles("+
					"ers_user_role_id serial primary key"+
					", user_role varchar(10)"+
				");"+
				"create table Ers_Reimbursement_Type("+
					"ers_type_id serial primary key"+
					", reimb_type varchar(10)"+
				");"+
				"create table Ers_Reimbursement_Status("+
					"id serial primary key"+
					", reimb_status varchar(10)"+
				");"+"create table Ers_Users(" + 
						"	ers_users_id serial primary key" + 
						"	, ers_username varchar(50) unique" + 
						"	, ers_password varchar(50)" + 
						"	, user_first_name varchar(100)" + 
						"	, user_last_name varchar(100)" + 
						"	, user_email varchar(150) unique" + 
						"	, user_role_id integer" + 
						");"+
				"create table Ers_Reimbursement(" + 
				"	reimb_id serial primary key" + 
				"	, reimb_amount integer" + 
				"	, reimb_submitted timestamp" + 
				"	, reimb_resolved timestamp" + 
				"	, reimb_description varchar(250)" + 
				"	, reimb_author integer" + 
				"	, reimb_resolver integer" + 
				"	, reimb_status_id integer" + 
				"	, reimb_type_id integer" + 
				");"+
				 "insert into Ers_Reimbursement values(default, 10, null, null, 'description', 1, 0,1,1);";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void h2DestroyDao() {
		try(Connection conn=
				DriverManager.getConnection(url,username, password))
		{
			String sql= "drop table ers_user_roles;"+
			"Drop table Ers_Reimbursement_Type; "+
			 "drop table Ers_Reimbursement_Status;"+
			 "drop table Ers_Users;"+
			 "drop table Ers_Reimbursement;";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	
	

}
