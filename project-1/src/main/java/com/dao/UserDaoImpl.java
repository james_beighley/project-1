package com.dao;

import com.model.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class UserDaoImpl implements UserDao {
	
	/**
	 * 
	 * @param email
	 * @param password
	 * @return User object specified by email and password, null if not in database
	 */
	
	String url = "";
	String username = "";
	String password = "";
	
	
	
	static {
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("try static block has fired");
        } catch (ClassNotFoundException e) {
            System.out.println("THE STATIC BLOCK HAS FAILED");
            e.printStackTrace();
        }
    }
	

	
	public UserDaoImpl() {
		
	}

	public UserDaoImpl(String _url, String _username, String _password) {
		url= _url;
		username = _username;
		password = _password;
	}

	@Override
	public User getUser(String email, String password1) {
		User toreturn = new User();
		
		try(Connection conn = DriverManager.getConnection(url, username,password)){
			System.out.println("in try jdbc block");
			String sql = "SELECT * FROM ers_users_roles_join WHERE user_email ="+"'"+email+"'";
					//WHERE user_email = "+ "'" +email+ "'"+ "  AND ers_password = "+ "'" +password1+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			//int u_i, String u, String p, String f, String l, String e, String r_i
			
			while(rs.next()) {
				System.out.println(rs.getInt(1));
				System.out.println(rs.getString(2));
				//toreturn = new User(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5), rs.getString(6),rs.getString(9));
				toreturn.setUser_id(rs.getInt(1));
				toreturn.setUsername(rs.getString(2));
				toreturn.setPassword(rs.getString(3));
				toreturn.setFirstname(rs.getString(4));
				toreturn.setLastname(rs.getString(5));
				toreturn.setEmail(rs.getString(6));
				toreturn.setRole(rs.getString(9));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		System.out.println(toreturn.getUser_id());
		System.out.println("returning User");
		return toreturn;

	}
	
	@Override
	public void h2InitDao() {
		
		try(Connection conn=
				DriverManager.getConnection(url,username, password))
		{
			String sql= ""+
			"create table Ers_User_Roles("+
					"ers_user_role_id serial primary key"+
					", user_role varchar(10)"+
				");"+
				"create table Ers_Users(" + 
						"	ers_users_id serial primary key" + 
						"	, ers_username varchar(50) unique" + 
						"	, ers_password varchar(50)" + 
						"	, user_first_name varchar(100)" + 
						"	, user_last_name varchar(100)" + 
						"	, user_email varchar(150) unique" + 
						"	, user_role_id integer references Ers_User_roles" + 
				");"+
				"create view ers_users_roles_join as select * from ers_users A inner join Ers_User_Roles B on A.user_role_id = B.ers_user_role_id ;"+
				"insert into Ers_User_Roles values(1, 'EMPLOYEE');"+
				"insert into Ers_Users values(1, 'myusername', 'mypassword', 'myfirstname','mylastname','myemail',1);";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void h2DestroyDao() {
		try(Connection conn=
				DriverManager.getConnection(url,username, password))
		{
			String sql= "drop view ers_users_roles_join;"+
			"Drop table Ers_Users; "+
			 "drop table Ers_User_Roles;";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

}
