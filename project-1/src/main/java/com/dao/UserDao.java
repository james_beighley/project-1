package com.dao;
import com.model.User;

public interface UserDao {

	/**
	 * 
	 * @param email
	 * @param password
	 * @return User object specified by email and password, null if not in database
	 */
	public User getUser(String email, String password);

	public void h2DestroyDao();

	public void h2InitDao();
}
