package com.dao;
import java.util.ArrayList;
import com.model.Reimbursement;

public interface ReimbursementDao {
	public ArrayList<Reimbursement> getAllReimbursements();
	public ArrayList<Reimbursement> getReimbursementsByEmail(String email);
	void createReimbursement(int amount, String description, String type, int authorid);
	public void resolveReimbursement(int reimbursementid, int resolverid, String resolved, int status_id);
	public int getStatusId(int reimbursementid);
	void h2DestroyDao();
	public void h2InitDao();
}
