package com.service;

import java.util.ArrayList;

import com.model.Reimbursement;

public interface ReimbursementService {
	public void createReimbursement(int amount, String description, String type, int authorid);
	public ArrayList<Reimbursement> getAllReimbursements();
	public ArrayList<Reimbursement> getReimbursementsByEmail(String email);
	public int getStatusId(int reimbursementid);
	public void resolveReimbursement(int reimbursementid, int resolverid, String resolved, int status_id);
	
}
