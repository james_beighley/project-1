package com.service;

import java.util.ArrayList;

import com.dao.ReimbursementDaoImpl;
import com.model.Reimbursement;

public class ReimbursementServiceImpl implements ReimbursementService {
	ReimbursementDaoImpl reimbursedao = new ReimbursementDaoImpl();
	@Override
	public void createReimbursement(int amount, String description, String type, int authorid) {
		reimbursedao.createReimbursement(amount, description, type, authorid);
	}

	@Override
	public ArrayList<Reimbursement> getAllReimbursements() {
		// TODO Auto-generated method stub
		return reimbursedao.getAllReimbursements();
	}

	@Override
	public ArrayList<Reimbursement> getReimbursementsByEmail(String email) {
		// TODO Auto-generated method stub
		return reimbursedao.getReimbursementsByEmail(email);
	}

	@Override
	public int getStatusId(int reimbursementid) {
		return reimbursedao.getStatusId(reimbursementid);
	}

	@Override
	public void resolveReimbursement(int reimbursementid, int resolverid, String resolved, int status_id) {
		reimbursedao.resolveReimbursement(reimbursementid, resolverid, resolved, status_id);
		
	}
	
	public ArrayList<Reimbursement> getAllReimbursementsWithStatus(String status) {
		return reimbursedao.getAllReimbursementsWithStatus(status);
	}

}
