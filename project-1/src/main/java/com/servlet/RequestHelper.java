package com.servlet;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.controller.HomeController;
import com.controller.LoginController;
import com.controller.ReimbursementController;
import com.fasterxml.jackson.core.JsonProcessingException;

public class RequestHelper {

	public static String process(HttpServletRequest request) throws JsonProcessingException, IOException {
		System.out.println("\t\tIn RequestHelper POST");
		System.out.println(request.getRequestURI());
		
		switch(request.getRequestURI()) {
		case "/project-1/forwarding/login":
			System.out.println("case1");
			return LoginController.login(request);
		case "/project-1/forwarding/home":
			System.out.println("case 2");
			return HomeController.home(request);
		case "/project-1/forwarding/new":
			return ReimbursementController.createReimbursement(request);
		case "/project-1/forwarding/resolve":
			return ReimbursementController.resolveReimbursement(request);
		case "/project-1/forwarding/signout":
			System.out.println("in signout forwarding servlet case");
			return LoginController.logout(request);
		default:
			System.out.println("bad checkpoint");
			return "/resources/html/badlogin.html";
		}
	}
}
