package com.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.controller.ReimbursementController;

public class AjaxRequestHelper {

	public static void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println(request.getRequestURI());

		switch (request.getRequestURI()) {
		case "/project-1/api/Ajax/allReimbursements":
			ReimbursementController.allFinder(request, response);
			break;
		case "/project-1/api/Ajax/someReimbursements":
			ReimbursementController.findByEmployee(request, response);
			break;
		case "/project-1/api/Ajax/pending":
			ReimbursementController.allPending(request, response);
			break;
			
		case "/project-1/api/Ajax/approved":
			ReimbursementController.allApproved(request, response);
			break;
			
		case "/project-1/api/Ajax/denied":
			ReimbursementController.allDenied(request, response);
			break;
		default:
			response.getWriter().println("null");
		}
	}
}