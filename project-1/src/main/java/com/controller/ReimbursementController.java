package com.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.model.Reimbursement;
import com.service.ReimbursementServiceImpl;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ReimbursementController {
	
	final static Logger loggy = org.apache.log4j.Logger.getLogger(LoginController.class);

	
	static ReimbursementServiceImpl reservice = new ReimbursementServiceImpl();

	public static void allFinder(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		/*
		 * THIS IS WHERE YOU'D GO TO THE DATABASE TO GET THE OBJECTS TO SEND TO THE CLIENT
		 */
		

		List<Reimbursement> reimbursements = reservice.getAllReimbursements();
		
		String tosend = "[";
		
		for(int i = 0; i<reimbursements.size(); i++) {
			tosend+= new ObjectMapper().writeValueAsString(reimbursements.get(i));
			if(i<reimbursements.size()-1)
				tosend+= ",";
		}
		tosend+= "]";
		
		//System.out.println(tosend);
		response.getWriter().write(tosend);
		//List<Reimbursement> newlist = Stream.of((reimbursements).collect(Collectors.toList()));
		

		//response.getWriter().write(new ObjectMapper().writeValueAsString(reimbursements));
		
	}
	
	public static void allPending(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		/*
		 * THIS IS WHERE YOU'D GO TO THE DATABASE TO GET THE OBJECTS TO SEND TO THE CLIENT
		 */
		

		List<Reimbursement> reimbursements = reservice.getAllReimbursementsWithStatus("PENDING");
		
		String tosend = "[";
		
		for(int i = 0; i<reimbursements.size(); i++) {
			tosend+= new ObjectMapper().writeValueAsString(reimbursements.get(i));
			if(i<reimbursements.size()-1)
				tosend+= ",";
		}
		tosend+= "]";
		
		//System.out.println(tosend);
		response.getWriter().write(tosend);
		//List<Reimbursement> newlist = Stream.of((reimbursements).collect(Collectors.toList()));
		

		//response.getWriter().write(new ObjectMapper().writeValueAsString(reimbursements));
		
	}
	
	public static void allDenied(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		/*
		 * THIS IS WHERE YOU'D GO TO THE DATABASE TO GET THE OBJECTS TO SEND TO THE CLIENT
		 */
		

		List<Reimbursement> reimbursements = reservice.getAllReimbursementsWithStatus("DENIED");
		
		String tosend = "[";
		
		for(int i = 0; i<reimbursements.size(); i++) {
			tosend+= new ObjectMapper().writeValueAsString(reimbursements.get(i));
			if(i<reimbursements.size()-1)
				tosend+= ",";
		}
		tosend+= "]";
		
		//System.out.println(tosend);
		response.getWriter().write(tosend);
		//List<Reimbursement> newlist = Stream.of((reimbursements).collect(Collectors.toList()));
		
		
		
		//in project 1 you ALREADY HAVE THE USER'S INFORMATION IN YOUR SESSION....NO NEED TO GO TO THE DATABASE
		//HttpSession session = req.getSession(); //extract the username password....then go to the DB using that username to
		//														get the reimbursmenets

		//response.getWriter().write(new ObjectMapper().writeValueAsString(reimbursements));
		
	}
	
	public static void allApproved(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		/*
		 * THIS IS WHERE YOU'D GO TO THE DATABASE TO GET THE OBJECTS TO SEND TO THE CLIENT
		 */
		

		List<Reimbursement> reimbursements = reservice.getAllReimbursementsWithStatus("APPROVED");
		
		String tosend = "[";
		
		for(int i = 0; i<reimbursements.size(); i++) {
			tosend+= new ObjectMapper().writeValueAsString(reimbursements.get(i));
			if(i<reimbursements.size()-1)
				tosend+= ",";
		}
		tosend+= "]";
		
		//System.out.println(tosend);
		response.getWriter().write(tosend);
		//List<Reimbursement> newlist = Stream.of((reimbursements).collect(Collectors.toList()));
		
		
		
		//in project 1 you ALREADY HAVE THE USER'S INFORMATION IN YOUR SESSION....NO NEED TO GO TO THE DATABASE
		//HttpSession session = req.getSession(); //extract the username password....then go to the DB using that username to
		//														get the reimbursmenets

		//response.getWriter().write(new ObjectMapper().writeValueAsString(reimbursements));
		
	}
	
	public static String createReimbursement(HttpServletRequest request) throws JsonProcessingException, IOException {
		int amount = Integer.parseInt(request.getParameter("amount"));
		String description = request.getParameter("description");
		String type = request.getParameter("type");
		int id = (int)request.getSession().getAttribute("userid");
		reservice.createReimbursement(amount, description, type, id);
		//System.out.println("this is my path info: "+ request.getPathInfo());
		loggy.info((String)request.getSession().getAttribute("username") + " has created a new reimbursement of type " + type);
		return "/resources/html/home.html";
		
	}
	
	public static String resolveReimbursement(HttpServletRequest request) throws JsonProcessingException, IOException {
		String resolved = request.getParameter("status");
		int reimbursementid = Integer.parseInt(request.getParameter("reimbursementid"));
		int status_id = reservice.getStatusId(reimbursementid);
		int resolverid = (int)request.getSession().getAttribute("userid");
		reservice.resolveReimbursement(reimbursementid, resolverid, resolved, status_id);
		loggy.info((String)request.getSession().getAttribute("username") + " has " +resolved +" reimbursement with ID " + reimbursementid);
		//System.out.println("this is my path info: "+ request.getPathInfo());
		return "/resources/html/managerhome.html";
		
	}
	
	public static void findByEmployee(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		/*
		 * THIS IS WHERE YOU'D GO TO THE DATABASE TO GET THE OBJECTS TO SEND TO THE CLIENT
		 */
		String email = (String)request.getSession().getAttribute("useremail");
		System.out.println(email);

		List<Reimbursement> reimbursements = reservice.getReimbursementsByEmail(email);
		
		String tosend = "[";
		
		for(int i = 0; i<reimbursements.size(); i++) {
			tosend+= new ObjectMapper().writeValueAsString(reimbursements.get(i));
			if(i<reimbursements.size()-1)
				tosend+= ",";
		}
		tosend+= "]";
		
		//System.out.println(tosend);
		response.getWriter().write(tosend);
		//List<Reimbursement> newlist = Stream.of((reimbursements).collect(Collectors.toList()));

		//response.getWriter().write(new ObjectMapper().writeValueAsString(reimbursements));
		
	}

}
