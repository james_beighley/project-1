package com.controller;

import java.util.Enumeration;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.service.UserServiceImpl;
import com.model.User;

public class LoginController {
	final static Logger loggy = org.apache.log4j.Logger.getLogger(LoginController.class);
	public static String login(HttpServletRequest request) {
		/*
		 * route guarding:
		 * 
		 * you may do stuff like "check if the user has an admin token in their session",
		 * "check if they are using the correct Http method",
		 * etc
		 */
		UserServiceImpl service = new UserServiceImpl();
		if(!request.getMethod().equals("POST")) {
			return "/index.html";
		}
		
		String username;

		String password;
		
		HttpSession session = request.getSession();
		Enumeration keys = request.getSession().getAttributeNames();
		while (keys.hasMoreElements()){
		   String key = (String)keys.nextElement();
		   System.out.println(key + ": " + request.getSession().getValue(key) + "<br>");
		}
		
		if(session.getAttribute("role")!=null) {
			if(session.getAttribute("role").equals("employee")) {
				System.out.println("in role employee section");
				return "/resources/html/home.html";
			}
			else {
				System.out.println("in role manager section");
				return "resources/html/managerhome.html";
			}
			
		}
		System.out.println("No session found");
		username = request.getParameter("username");
		password = request.getParameter("password");
		
			
		System.out.println(username);
		User user = service.getUser(username, password);
		loggy.setLevel(Level.ALL);
		if(loggy.isInfoEnabled())
			loggy.info(user.getRole() + " " + user.getUsername() + " has signed in.");

		//check to see if the user has the correct username and password
		if(!(username.equals(user.getEmail()) & password.equals(user.getPassword()))) {
			
			System.out.println("block1");
			return "/forwarding/incorrectcredentials";
		}else {
			session.setAttribute("useremail", user.getEmail());
			session.setAttribute("username", user.getUsername());
			session.setAttribute("userid", user.getUser_id());
			session.setAttribute("password", user.getPassword());
			session.setAttribute("role", user.getRole());
			if(user.getRole().equals("employee")) {
				System.out.println("block2");
				return "/resources/html/home.html";
			}
				
			else {
				System.out.println("block3");
				return "/resources/html/managerhome.html";
			}
				
		}
	}
	
	public static String logout(HttpServletRequest request) {
		loggy.info((String)request.getSession().getAttribute("username") + " has signed out");
		request.getSession().invalidate();
		System.out.println("Session has been invalidated");
		
		return "/index.html";
	}

}

